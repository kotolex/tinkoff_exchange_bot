from typing import List
from datetime import datetime
from logging import Logger, FileHandler, Formatter

from requests import Session

from base.exceptions import HttpError
from base.instruments import Stock, Bond, Instrument, Etf, Currency, OrderBook, Candle


class TinkoffExchangeBot:
    """
    Класс-родитель для ботов(реального или для песочницы). Подразумевает, что у юзера есть токен, получить его можно
    вот тут https://tinkoffcreditsystems.github.io/invest-openapi/auth/
    """

    cache = {Stock: None, Bond: None, Etf: None, Currency: None}

    def __init__(self, token: str):
        self.token = token
        self.url = 'https://api-invest.tinkoff.ru/openapi'
        self.headers = {'Authorization': f"Bearer {token}", 'Accept': 'application/json'}
        self.session = Session()
        self.session.headers.update(self.headers)
        self.logger: Logger = self._get_logger('TinkoffExchangeBot')
        self.account_id: str = ''

    def _get_logger(self, name: str) -> Logger:
        logger = Logger(name)
        formatter = Formatter("%(asctime)s %(levelname)s: %(message)s", datefmt="%B %dth %H:%M %p")
        handler = FileHandler(f'{datetime.now():%d_%m_%Y__%H_%M_%S}.log', encoding='utf-8')
        handler.setFormatter(formatter)
        logger.addHandler(handler)
        return logger

    def get_currencies_balance(self) -> List[dict]:
        """
        Получение баланса по всем валютам в наличии у пользователя
        :return: список словарей вида [{'currency': 'EUR', 'balance': 0}, {'currency': 'RUB', 'balance': 1000.0},
        {'currency': 'USD', 'balance': 100.0}]
        """
        message = f'Try to get currencies balance'
        url = f'{self.url}/portfolio/currencies/?brokerAccountId={self.account_id}'
        response = self._send(message, 'get', url=url)
        return response.json()['payload']['currencies']

    def portfolio(self) -> List[Instrument]:
        """
        Получение портфеля пользователя (все позиции)
        :return: список текущих инструментов пользователя
        """
        message = f'Try to get user portfolio'
        url = f'{self.url}/portfolio/?brokerAccountId={self.account_id}'
        response = self._send(message, 'get', url=url)
        return [Instrument(json) for json in response.json()['payload']['positions']]

    def orders(self) -> List:
        """
        Получение списка активных заявок пользователя
        :return: список заявок
        """
        message = f'Try to get user orders'
        url = f'{self.url}/orders/?brokerAccountId={self.account_id}'
        response = self._send(message, 'get', url=url)
        return response.json()['payload']

    def user_accounts(self) -> List[dict]:
        """
        Возвращает список брокерских счетов пользователя
        :return: список словарей вида {'brokerAccountType': 'Tinkoff', 'brokerAccountId': 'ID123456'}
        """
        message = f'Try to get client accounts'
        url = f'{self.url}/user/accounts'
        response = self._send(message, 'get', url=url)
        return response.json()['payload']['accounts']

    def stocks(self, use_cache: bool = True) -> List[Instrument]:
        """
        Получение списка всех акций. Каждая акция представлена объектом Stock вида
        Stock (figi=BBG000HLJ7M4, ticker=IDCC, isin=US45867G1013, minPriceIncrement=0.01, lot=1, currency=USD,
        name=InterDigItal Inc)
        :return: список акицй в продаже
        """
        message = f'Try to get all stocks on market'
        url = f'{self.url}/market/stocks'
        return self._instrument(Stock, message, url, use_cache=use_cache)

    def bonds(self, use_cache: bool = True) -> List[Instrument]:
        """
        Получение списка всех облигаций. Каждая облигация представлена объектом Bond вида
        Bond (figi=BBG00T22WKV5, ticker=SU29013RMFS8, isin=RU000A101KT1, minPriceIncrement=0.01, lot=1, currency=RUB,
        faceValue=1000.0, name=ОФЗ 29013)
        :return: список облигаций в продаже
        """
        message = f'Try to get all bonds on market'
        url = f'{self.url}/market/bonds'
        return self._instrument(Bond, message, url, use_cache=use_cache)

    def etfs(self, use_cache: bool = True) -> List[Instrument]:
        """
        Получение списка всех etf. Каждая etf представлена объектом ETFS вида
        ETFS (figi=BBG333333333, ticker=TMOS, isin=RU000A101X76, minPriceIncrement=0.002, lot=1, currency=RUB,
        name=Тинькофф iMOEX)
        :return: список etfs в продаже
        """
        message = f'Try to get all etfs on market'
        url = f'{self.url}/market/etfs'
        return self._instrument(Etf, message, url, use_cache=use_cache)

    def currencies(self, use_cache: bool = True) -> List[Instrument]:
        """
        Получение списка всех валютных пар на рынке. Каждая валюта представлена объектом Currency вида
        Currency (figi=BBG0013HGFT4, ticker=USD000UTSTOM, isin=, minPriceIncrement=0.0025, lot=1000, currency=RUB,
        name=Доллар США)
        :return: список валют в продаже
        """
        message = f'Try to get all currencies on market'
        url = f'{self.url}/market/currencies'
        return self._instrument(Currency, message, url, use_cache=use_cache)

    def orderbook(self, figi: str, depth: int) -> OrderBook:
        """
        Получение стакана по FIGI
        :param figi: строковое поле для инструмента
        :param depth: глубина от 1 до 20
        :return: объект OrderBook
        """
        if depth < 1 or depth > 20:
            raise ValueError(f'Depth must be in range 1-20, but got {depth}')
        message = f'Try to get orderbook by figi={figi}  and depth={depth} on market'
        url = f'{self.url}/market/orderbook?figi={figi}&depth={depth}'
        response = self._send(message, 'get', url=url)
        return OrderBook(response.json()['payload'])

    def candles(self, figi: str, from_date: str, to_date: str, interval: str) -> List[Instrument]:
        """
        Получение свечи для инструмента с определенным интервалом
        :param figi: строковое поле для инструмента
        :param from_date: начиная с даты (например 2020-12-27T15:34:26.558279+00:00)
        :param to_date: до даты (например 2021-01-03T15:34:26.558279+00:00)
        :param interval: интервал из допустимых ('1min', '2min', '3min', '5min', '10min', '15min', '30min', 'hour',
        'day', 'week', 'month')
        :return: список свеч для инструмента
        """
        allowed = ('1min', '2min', '3min', '5min', '10min', '15min', '30min', 'hour', 'day', 'week', 'month')
        if interval not in allowed:
            raise ValueError(f'Not allowed interval {interval}, choose one of {allowed}')
        # TODO исключения на даты с поясами
        message = f'Try to get candles by figi={figi} with interval={interval} on market'
        url = f'{self.url}/market/candles'
        params = {'figi': figi, 'from': from_date, 'to': to_date, 'interval': interval}
        return self._instrument(Candle, message, url, params)

    def instrument_by_figi(self, figi: str) -> Instrument:
        """
        Получение конкретного инструмента по его figi идентификатору
        :param figi: идентификатор инструмента
        :return: конкретный объект
        """
        message = f'Try to get instrument by figi={figi} on market'
        url = f'{self.url}/market/search/by-figi'
        params = {'figi': figi}
        response = self._send(message, 'get', url=url, params=params)
        return Instrument.create(response.json()['payload'])

    def instrument_by_ticker(self, ticker: str) -> List[Instrument]:
        """
        Получение списка инструментов по  тикеру
        :param ticker: идентификатор инструмента
        :return: список объектов с таким тикером
        """
        message = f'Try to get instrument by ticker={ticker} on market'
        url = f'{self.url}/market/search/by-ticker'
        params = {'ticker': ticker}
        response = self._send(message, 'get', url=url, params=params)
        return [Instrument.create(json) for json in response.json()['payload']['instruments']]

    def _instrument(self, instrument: type, message: str, url: str, params: dict = None,
                    use_cache: bool = True) -> List[Instrument]:
        if instrument in TinkoffExchangeBot.cache and TinkoffExchangeBot.cache[instrument] and use_cache:
            return TinkoffExchangeBot.cache[instrument]
        response = self._send(message, 'get', url=url, params=params)
        result = [instrument(a_dict) for a_dict in response.json()['payload']['instruments']]
        if instrument in TinkoffExchangeBot.cache and use_cache:
            TinkoffExchangeBot.cache[instrument] = result
        return result

    def _send(self, message: str, method: str = 'post', **kwargs):
        self.logger.info(message)
        params = kwargs.get('params')
        if method == 'post':
            self.logger.info(f'    POST request to {kwargs["url"]} with body {kwargs["body"]}')
            response = self.session.post(kwargs["url"], json=kwargs["body"], params=params)
        else:
            self.logger.info(f'    GET request to {kwargs["url"]}')
            response = self.session.get(kwargs["url"], params=params)
        self.logger.info(f'    Response from {kwargs["url"]} is {response.status_code} and body {response.text}')
        if response.json()['status'] != 'Ok':
            self.logger.error(f'    Response from {kwargs["url"]} is not Ok!')
        if response.status_code != 200:
            raise HttpError(f'Status code id {response.status_code}, and text is "{response.text}"')
        return response


class RealBot(TinkoffExchangeBot):
    """
    Представляет собой реализацию для реальной торговли, не использует песочницу. Этому боту нужен настоящий токен!
    """

    def __init__(self, token: str):
        super().__init__(token)
        self.logger.info(f'Starting real bot, all operations performs on real money')


class SandboxBot(TinkoffExchangeBot):
    """
    Реализация для песочницы, не осуществляет реальной торговли, нужен токен для песочницы. Ограничения для него можно
    смотреть вот тут https://tinkoffcreditsystems.github.io/invest-openapi/env/
    """

    def __init__(self, token: str):
        super().__init__(token)
        self.url = 'https://api-invest.tinkoff.ru/openapi/sandbox'
        self.logger = self._get_logger('TinkoffExchangeBot(Sandbox)')
        self.logger.info(f'Starting bot, using sandbox! No real trades performed')
        self.account_id = self._register()

    def _register(self) -> str:
        message = 'Try to register token at sandbox'
        body = {'brokerAccountType': 'Tinkoff'}
        url = f'{self.url}/sandbox/register'
        response = self._send(message, body=body, url=url)
        return response.json()['payload']['brokerAccountId']

    def set_currency_balance(self, currency: str, amount: float):
        """
        Устанавливает баланс в указанной валюте, работает только в песочнице
        :param currency: название валюты (RUB, USD, EUR)
        :param amount: новый баланс, должен быть не отрицательным
        """
        if amount < 0:
            raise ValueError(f'Amount must be non-negative, but got {amount}')
        allowed = ('RUB', 'USD', 'EUR')
        if currency not in allowed:
            raise ValueError(f'Wrong currency {currency}, allowed are {allowed}')
        message = f'Try to set balance ({amount} of {currency})'
        body = {'currency': currency, 'balance': amount}
        url = f'{self.url}/sandbox/currencies/balance/?brokerAccountId={self.account_id}'
        self._send(message, body=body, url=url)

    def set_instrument_balance(self, figi: str, amount: float):
        """
        Устанавливает баланс указанного инструмента, работает только в песочнице
        :param figi: идентификатор инструмента
        :param amount: новый баланс, должен быть не отрицательным
        """
        if amount < 0:
            raise ValueError(f'Amount must be non-negative, but got {amount}')
        message = f'Try to set balance ({amount} for figi {figi})'
        body = {'figi': figi, 'balance': amount}
        url = f'{self.url}/sandbox/positions/balance?brokerAccountId={self.account_id}'
        self._send(message, body=body, url=url)

    def clear_all_positions(self):
        """
        Удаляет все позиции (инструменты) и балансы.
        """
        message = f'Try to remove all instruments'
        url = f'{self.url}/sandbox/clear?brokerAccountId={self.account_id}'
        self._send(message, body='', url=url)


def start_bot(token: str, use_sandbox: bool = True) -> TinkoffExchangeBot:
    """
    Метод получения бота для работы с биржей Тинькофф
    :param token: токен, валидный для песочницы или реальной торговли
    :param use_sandbox: если True то вернет бота для работы с песочницей, иначе реального бота
    :return: объект бота для торговли на бирже
    """
    if not token:
        raise ValueError('Token cant be empty! See more https://tinkoffcreditsystems.github.io/invest-openapi/auth/')
    return SandboxBot(token) if use_sandbox else RealBot(token)


if __name__ == '__main__':
    import time

    with open('secret.txt') as file:
        tok = file.read().strip()
    bot: SandboxBot = start_bot(tok)
    start = time.time()
    s = bot.stocks()[0]
    print(time.time() - start)
    start = time.time()
    s1 = bot.stocks()[0]
    print(time.time() - start)

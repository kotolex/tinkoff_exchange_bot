from base.interfaces import Observer, Observable


class Actor(Observer):
    def __init__(self):
        self.info = []

    def push_data(self, info):
        self.info.append(f'Got info "{info}"')


class Analyzer(Observer, Observable):
    def __init__(self):
        super().__init__()
        self.data = []

    def analize(self):
        if len(self.data) < 6:
            return
        *_, one, two, three, four, five, six = self.data
        if four >= one and four > two and five > three and five > two and five > one and six >= three and six >= four \
                and six > one and six > two:
            self.notify('+ trend')
            return
        if one >= five and one >= three and one > four and six < one and six < two and three >= five:
            self.notify('- trend')
            return

    def push_data(self, *args):
        self.data.append(*args)
        self.analize()

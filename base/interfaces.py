class Observer:

    def push_data(self, *args):
        pass


class Observable:
    def __init__(self):
        self.observers = []

    def register(self, observer: Observer):
        self.observers.append(observer)

    def notify(self, *args):
        for observer in self.observers:
            observer.push_data(*args)

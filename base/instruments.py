class Instrument:
    def __init__(self, json: dict):
        self._name = 'Instrument'
        self.figi: str = ''
        self.ticker: str = ''
        self.__dict__.update(json)

    def __str__(self):
        values = (f'{k}={v}' for k, v in self.__dict__.items() if not k.startswith('_') and k != 'type')
        return f'{self._name} ({", ".join(values)})'

    def __repr__(self):
        return self.__str__()

    @staticmethod
    def create(json: dict):
        type_ = json.get('type')
        if not type_:
            return Instrument(json)
        cases = {'Stock': Stock, 'Bond': Bond, 'Etf': Etf, 'Currency': Currency}
        if type_ not in cases:
            return Instrument(json)
        return cases[type_](json)


class Stock(Instrument):
    def __init__(self, json: dict):
        super().__init__(json)
        self._name = 'Stock'


class Bond(Instrument):
    def __init__(self, json: dict):
        super().__init__(json)
        self._name = 'Bond'
        self.faceValue: float = 1000.0


class Etf(Instrument):
    def __init__(self, json: dict):
        super().__init__(json)
        self._name = 'Etf'


class Currency(Instrument):
    def __init__(self, json: dict):
        super().__init__(json)
        self._name = 'Currency'


class OrderBook(Instrument):
    def __init__(self, json: dict):
        self.depth: int = 1
        self.tradeStatus: str = 'NotAvailableForTrading'
        self.lastPrice: float = 0.0
        self.closePrice: float = 0.0
        self.limitUp: float = 0.0
        self.limitDown = 0.0
        self.bids: list = []
        self.asks: list = []
        super().__init__(json)
        del self.ticker
        self._name = 'OrderBook'


class Candle(Instrument):
    def __init__(self, json: dict):
        self.time: str = ''
        self.interval: str = ''
        super().__init__(json)
        del self.ticker
        self._name = 'Candle'

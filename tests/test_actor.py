from unittest import TestCase, main
from base.algo import Actor, Observable, Observer, Analyzer


class TestActor(TestCase):
    def setUp(self) -> None:
        self.actor = Actor()
        self.obs = Observable()
        analyzer = Analyzer()
        self.obs.register(analyzer)
        analyzer.register(self.actor)

    def test_raising(self):
        raising = (1, 0, 2, 1, 3, 2)
        for e in raising:
            self.obs.notify(e)
        self.assertEqual(['Got info "+ trend"'], self.actor.info)

    def test_raising_straight(self):
        raising = (1, 2, 3, 4, 6, 7)
        for e in raising:
            self.obs.notify(e)
        self.assertEqual(['Got info "+ trend"'], self.actor.info)

    def test_falling(self):
        falling = (1, 2, 1, 0, 1, 0)
        for e in falling:
            self.obs.notify(e)
        self.assertEqual(['Got info "- trend"'], self.actor.info)

    def test_falling_straight(self):
        falling = (6, 5, 4, 3, 2, 1)
        for e in falling:
            self.obs.notify(e)
        self.assertEqual(['Got info "- trend"'], self.actor.info)


if __name__ == '__main__':
    main()
